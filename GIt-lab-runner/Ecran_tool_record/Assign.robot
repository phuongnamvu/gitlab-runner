*** Settings ***
Documentation     A test suite with a single test for test-v64-rc.blueway.fr:8080/BWDesignerFaces/login.jsf
...               Created by hats' Robotcorder
Library           SeleniumLibrary    timeout=10

*** Variables ***
${BROWSER}    chrome
${SLEEP}    3

*** Test Cases ***
test-v64-rc.blueway.fr:8080/BWDesignerFaces/login.jsf test
    
    # We can use localhost - without Sleep
    # We can use http      - with    Sleep  

    Set Selenium Speed    0.8
    Open Browser    http://localhost:48080/BWDesignerFaces/login.jsf   ${BROWSER}
    
    Click Element   //input[@name="loginForm:username"]
    Input Text    //input[@name="loginForm:username"]    admin
    
    Click Element    //input[@name="loginForm:password"]
    Input Text    //input[@name="loginForm:password"]    admin
    

    Click Element    //span[@class="ui-button-text ui-c"]
    #Sleep    1 
    Click Element    xpath=(//span[@class="ui-button-text ui-c"])[42]
    Click Element    xpath=(//span[@class="Menuitemname ui-draggable ui-draggable-handle"])[42]
    #Sleep    2    
    
    

    # Nút New sửa tay 
    Click Element          xpath=(//span[@class="ui-treenode-icon ui-icon bwicon-support"])[38]
    #Click Element    xpath=(//label[@class="datakey customDrag"])[58]
    
    # Nút new Tự động : Tool  - Khoảng trắng - Hư 
    #Click Element          xpath=(//label[@class="datakey customDrag  "])[58]
    #Sleep    2    
    #Click Element    //input[@name="tabscontent:tabView:edittext_0_0"]    
    Input Text    //input[@name="tabscontent:tabView:edittext_0_0"]    Nam_robot_ecran
    Click Element    //span[@class="ui-button-icon-left ui-icon ui-c fa fa-floppy-o faBigButtonIcon"]

    Close Browser
    

