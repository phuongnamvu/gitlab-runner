

*** Settings ***
Variables       ../variables/global1.py
Resource        ../resources/setup.resource
Test Setup     NONE 
Test Teardown  NONE 

Variables    ../variables/global1.py

*** Test cases ***
test All machines
    [Template]    Test a machine
    Debian9
#    Centos7
#    vagrant_centos6
#    vagrant_debian7 
#    vagrant_debian8

*** Variables ***
${source}           ${projectRoot}/${vagrantRoot}
${cible}            ${projectRoot}/${tempDirectory}


${Nam}              projectRoot/vagrantRoot
${Cyrille }         projectRoot/tempDirectory   


# Global1.py  Example 
${projectRoot}      "/home/blueway/Projects"
${vagrantRoot}       "vagrant-env" 
${tempDirectory}     "blueway-quality/arena"
${setupPath}         "/home/blueway/Projects/blueway-quality/sources/setupfiles/Setup.zip"


*** Keywords ***
Test a machine
    [Documentation]         Copy a configured machine and launch it
    [Arguments]             ${machine}
    Copy a Machine          ${source}    ${cible}    ${machine}
    Copy a setupfile        ${cible}     ${machine}
    Vagrant up              ${cible}     ${machine}
    Scp files to Machine    ${cible}     ${machine}   ./Setup.zip
    Scp files to Machine    ${cible}     ${machine}   ./conf/
    Scp files to Machine    ${cible}     ${machine}   ./provision.sh
    Ssh setup to Machine    ${cible}     ${machine}   chmod +x /home/vagrant/provision.sh
    Ssh setup to Machine    ${cible}     ${machine}   sudo /home/vagrant/provision.sh
    # Clean a Machine         ${cible}     ${machine}

