
*** setting ***
Library    SeleniumLibrary 


# After : Addd Selenium : OK 
# Version English : # button[alt='supportmemory']
# Version FR  # button[alt='supportmemory'] > .ui-button-text.ui-c

*** Variables *** 




*** Test Cases ***

Assign 
    OpenBroswer
    NewService
    NewMemory 
     
    DragdropMemory 
    OpenService
    EditService
    MapField_root
    Mapfield_assign
    
*** Keywords ***

OpenBroswer 
    Open Browser    http://localhost:48080/BWDesignerFaces/login.jsf    Chrome
    Maximize Browser Window
    
    Click Element    //input[@name="loginForm:username"]  
    Input Text    //input[@name="loginForm:username"]    admin
    
    
    Click Element   //input[@name="loginForm:password"]
    Input Text    //input[@name="loginForm:password"]    admin
    
    
    Click Element    //span[@class="ui-button-text ui-c"]

NewService 
    
    Maximize Browser Window
    Wait Until Page Contains Element    id=rigthmenu:idTopMenuItem:7:menuButton     
    Click Element    id=rigthmenu:idTopMenuItem:7:menuButton     
    Sleep    3        
    #Click Element    xpath=(//SPAN[@class='Menuitemname ui-draggable ui-draggable-handle'][text()='Service'][text()='Service'])[2]            
    
    Click Element    xpath=(//SPAN[text()='Service'])[2]         
    
    Sleep    2      
    #Relative Xpath helper 
    Click Element        xpath=(//LABEL[@title=''][text()='Nouveau'][text()='Nouveau'])[33]  
    
                                
   
    Sleep    2   
    Wait Until Page Contains Element     id=tabscontent:tabView:edittext_0_0     
    Click Element    id=tabscontent:tabView:edittext_0_0    
    Input Text    id=tabscontent:tabView:edittext_0_0        Service_robot
    
    #Input Text    Xpath=//input[@id='tabscontent:tabView:edittext_0_12']    Test 
    
    #Click Element    xpath=//label[@id='tabscontent:tabView:combobox_0_14_label']  
    
    #Click Element    xpath=//li[@id='tabscontent:tabView:combobox_0_14_1']
    
    #Click Element    xpath=//label[@id='tabscontent:tabView:combobox_0_15_label']    
    
    #Click Element    xpath=//li[@id='tabscontent:tabView:combobox_0_15_1']            

    
    Click Element    css=.fa-floppy-o
    
NewMemory 
    Set Selenium Speed    0.8
    Maximize Browser Window
    Open Browser    http://localhost:48080/BWDesignerFaces/login.jsf    Chrome
    
    
    Click Element    //input[@name="loginForm:username"]  
    Input Text    //input[@name="loginForm:username"]    admin
    
    
    Click Element   //input[@name="loginForm:password"]
    Input Text    //input[@name="loginForm:password"]    admin
    
    
    Click Element    //span[@class="ui-button-text ui-c"]

    Sleep  1
    Click Element    xpath=(//span[@class="ui-button-text ui-c"])[39]
    Sleep  1 
    Click Element    xpath=(//span[@class="Menuitemname ui-draggable ui-draggable-handle"])[17]


    #Click Element    xpath=(//label[@class="datakey customDrag  "])[35]
    Click Element    xpath=(//span[@class="ui-treenode-icon ui-icon bwicon-support"])[14]

    Input Text    //input[@name="tabscontent:tabView:supportMem_combobox_0_2_editableInput"]    BBB
    Input Text    //input[@name="tabscontent:tabView:supportMem_edittext_0_0"]    Me

    Click Element    //td[@class="ui-editable-column"]
    #Input Text    //input[@name="tabscontent:tabView:dynamicColumnsTable_0:0:j_idt1240:0:j_idt1244"]    a

    Input Text   //tbody[@id='tabscontent:tabView:dynamicColumnsTable_0_data']/tr[@role='row']/td[1]/div[@class='ui-cell-editor']//input[@role='textbox']       a
                 
    Click Element    xpath=(//span[@class="ui-icon ui-icon-triangle-1-s ui-c"])[8]
   
    Click Element    xpath=(//span[@class="ui-icon ui-icon-triangle-1-s ui-c"])[8]
    
    
    Click Element    //li[@class="ui-selectonemenu-item ui-selectonemenu-list-item ui-corner-all" and text()="Alphanumeric"]

    


    Click Element    xpath=(//td[@class="ui-editable-column"])[5]
    Click Element    xpath=(//td[@class="ui-editable-column"])[5]
    
    Input Text    //tbody[@id='tabscontent:tabView:dynamicColumnsTable_0_data']/tr[2]/td[1]/div[@class='ui-cell-editor']//input[@role='textbox']    b
    
   

    Click Element    xpath=(//span[@class="ui-icon ui-icon-triangle-1-s ui-c"])[15]
    Click Element    xpath=(//span[@class="ui-icon ui-icon-triangle-1-s ui-c"])[15]
    Click Element    //li[@class="ui-selectonemenu-item ui-selectonemenu-list-item ui-corner-all" and text()="Alphanumeric"]
    Click Element    //span[@class="ui-button-icon-left ui-icon ui-c fa fa-floppy-o faBigButtonIcon"]

   

    
DragdropMemory 
  
    Click Element    xpath=(//span[@class="ui-button-text ui-c"])[39]
    Click Element    xpath=(//span[@class="Menuitemname ui-draggable ui-draggable-handle"])[17]
    Sleep   1
    Drag And Drop By Offset   xpath=(//label[text()="BBB"])[1]             150      300
    
      
      
    #Click Element    xpath=(//label[text()="AAAAA"])[1]

OpenService
    Maximize Browser Window
    Wait Until Page Contains Element    id=rigthmenu:idTopMenuItem:7:menuButton     
    Click Element    id=rigthmenu:idTopMenuItem:7:menuButton     
    Sleep    3        
    #Click Element    xpath=(//SPAN[@class='Menuitemname ui-draggable ui-draggable-handle'][text()='Service'][text()='Service'])[2]            
    
    Click Element    xpath=(//SPAN[text()='Service'])[2]         
    Click Element    xpath=(//label[text()="Service_robot - 1"])[1] 
    
EditService
    #Click Element    //a[text()="Editor"]
    Click Element    //i[@class='fa fa-pencil-square-o faLinkMenuIcon']

    


MapField_root
    Drag And Drop    locator    target
    
Mapfield_assign
  
    Drag And Drop    locator    target
    





