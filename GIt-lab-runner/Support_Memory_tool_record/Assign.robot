*** Settings ***
Documentation     A test suite with a single test for localhost:48080/BWDesignerFaces/login.jsf
...               Created by hats' Robotcorder
Library           SeleniumLibrary    timeout=10

*** Variables ***
${BROWSER}    chrome
${SLEEP}    3

*** Test Cases ***
localhost:48080/BWDesignerFaces/login.jsf test
    
    Set Selenium Speed    0.8
    Open Browser    http://localhost:48080/BWDesignerFaces/login.jsf    ${BROWSER}
    
    
    Click Element    //input[@name="loginForm:username"]  
    Input Text    //input[@name="loginForm:username"]    admin
    
    
    Click Element   //input[@name="loginForm:password"]
    Input Text    //input[@name="loginForm:password"]    admin
    
    
    Click Element    //span[@class="ui-button-text ui-c"]

    Sleep  1
    Click Element    xpath=(//span[@class="ui-button-text ui-c"])[39]
    Sleep  1 
    Click Element    xpath=(//span[@class="Menuitemname ui-draggable ui-draggable-handle"])[17]


    #Click Element    xpath=(//label[@class="datakey customDrag  "])[35]
    Click Element    xpath=(//span[@class="ui-treenode-icon ui-icon bwicon-support"])[14]

    Input Text    //input[@name="tabscontent:tabView:supportMem_combobox_0_2_editableInput"]    BBB
    Input Text    //input[@name="tabscontent:tabView:supportMem_edittext_0_0"]    Me

    Click Element    //td[@class="ui-editable-column"]
    Input Text    //input[@name="tabscontent:tabView:dynamicColumnsTable_0:0:j_idt1240:0:j_idt1244"]    a

   
    Click Element    xpath=(//span[@class="ui-icon ui-icon-triangle-1-s ui-c"])[8]
   
    Click Element    xpath=(//span[@class="ui-icon ui-icon-triangle-1-s ui-c"])[8]
    
    
    Click Element    //li[@class="ui-selectonemenu-item ui-selectonemenu-list-item ui-corner-all" and text()="Alphanumeric"]

    


    Click Element    xpath=(//td[@class="ui-editable-column"])[5]
    Click Element    xpath=(//td[@class="ui-editable-column"])[5]
    
    Input Text    //input[@name="tabscontent:tabView:dynamicColumnsTable_0:1:j_idt1240:0:j_idt1244"]    b
    
    Click Element    xpath=(//span[@class="ui-icon ui-icon-triangle-1-s ui-c"])[15]
    Click Element    xpath=(//span[@class="ui-icon ui-icon-triangle-1-s ui-c"])[15]
    Click Element    //li[@class="ui-selectonemenu-item ui-selectonemenu-list-item ui-corner-all" and text()="Alphanumeric"]
    Click Element    //span[@class="ui-button-icon-left ui-icon ui-c fa fa-floppy-o faBigButtonIcon"]

    Close Browser