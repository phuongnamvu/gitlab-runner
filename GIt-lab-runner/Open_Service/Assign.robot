
*** setting ***
Library    SeleniumLibrary 


# After : Addd Selenium : OK 
# Version English : # button[alt='supportmemory']
# Version FR  # button[alt='supportmemory'] > .ui-button-text.ui-c

*** Variables *** 




*** Test Cases ***

OpenService
    OpenBroswer 
    OpenService
    EditService
    
    Dragdrop_memory
    MapField_root
*** Keywords ***

OpenBroswer 
    
  
    Open Browser    http://localhost:48080/BWDesignerFaces/login.jsf    Chrome
    Maximize Browser Window
    
    Click Element    //input[@name="loginForm:username"]  
    Input Text    //input[@name="loginForm:username"]    admin
    
    
    Click Element   //input[@name="loginForm:password"]
    Input Text    //input[@name="loginForm:password"]    admin
    
    
    Click Element    //span[@class="ui-button-text ui-c"]





OpenService
    Maximize Browser Window
    Wait Until Page Contains Element    id=rigthmenu:idTopMenuItem:7:menuButton     
    Click Element    id=rigthmenu:idTopMenuItem:7:menuButton     
    Sleep    3        
    #Click Element    xpath=(//SPAN[@class='Menuitemname ui-draggable ui-draggable-handle'][text()='Service'][text()='Service'])[2]            
    
    Click Element    xpath=(//SPAN[text()='Service'])[2]         
    Click Element    xpath=(//label[text()="Service_robot - 1"])[1] 
    
EditService
    #Click Element    //a[text()="Editor"]
    Sleep  1
    #Click Element    //i[@class='fa fa-pencil-square-o faLinkMenuIcon']
    Click Element   css:.menuOncontext.ui-outputpanel.ui-widget > a:nth-of-type(2)

    Sleep    1
    Click Element    //span[text()='Service Service_robot']

Dragdrop_memory
    
    Click Element    xpath=(//span[@class="ui-button-text ui-c"])[39]
    Sleep   1
    Click Element    xpath=(//span[@class="Menuitemname ui-draggable ui-draggable-handle"])[17]
    Sleep   1
    Drag And Drop By Offset   xpath=(//label[text()="BBB"])[1]             150      300



MapField_root
    
    Sleep    1
    Click Element    css:[data-rowkey='0_0'] > [aria-expanded] [class='ui-tree-toggler ui-icon ui-icon-triangle-1-e']  
    Sleep   1 
    
    Click Element    css:[data-rowkey='0_0_0'] [class='ui-tree-toggler ui-icon ui-icon-triangle-1-e']
    
    Sleep   1 
    
    Click Element    css:td:nth-of-type(1) > .ui-cell-editor > .ui-cell-editor-input > input[role='textbox']
    Sleep  1  
    Drag And Drop   css:label[title='BBB.Me.a']       css:td:nth-of-type(1) > .ui-cell-editor > .ui-cell-editor-input > input[role='textbox']
    
  


